import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  constructor(private http: HttpClient) { }

  findAll() {
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; charset=utf-8');
    return this.http.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs',{headers});
  }

  createAll(articles) {
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; charset=utf-8');
    return this.http.post(environment.api+ 'article',articles );
  }

  find() {
    return this.http.get(environment.api + 'article');
  }

  delete(id){
    return this.http.delete(environment.api + 'article/' + id);
  }

}
