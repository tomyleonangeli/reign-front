import { Component } from '@angular/core';
import { ArticlesService } from './services/articles.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'reign-app-front';

  articlesList: any = [];
  constructor(private articles: ArticlesService, private spinner: NgxSpinnerService) {

    this.spinner.show();
    this.findAllFromApi();
    setTimeout(() => {
      this.spinner.hide();
      this.findAllFromDataBase();
    }, 3000);

    setTimeout(() => {
      location.reload();
    }, 3600000);

  }

  findAllFromApi() {
    this.articles.findAll().subscribe(data => {
      this.articles.createAll(data['hits']).subscribe(insert => {

      });
    })
  }

  findAllFromDataBase() {
    this.articles.find().subscribe(data => {
      this.articlesList = data;
      this.spinner.hide();
    })
  }

  delete(article) {

    this.articles.delete(article).subscribe(data => {
      this.spinner.show();
      this.articlesList = null;
      this.findAllFromDataBase();
    });
  }


}
